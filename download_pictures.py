# -*- coding: utf-8 -*-
import re
import os
import time
import yaml
import json
import requests

proxy = '59.32.45.128:4236'
real_proxy = {
    'http': proxy,
    'https': proxy
}

DATA_DIR = r'C:\Users\Administrator\Desktop\tk_pic\\'
authorEid = '3xgah2w6ymxy5xq'

urls = [
    "https://js2.a.yximgs.com/upic/2019/05/22/00/BMjAxOTA1MjIwMDAwNTFfOTUzNTE0OTMzXzEzMjkzMjMzMDU2XzJfNg==_B5ac6cef227e9d6d81e577cb4e1fd52c0.jpg",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjUzMzc0NzA3_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjUzMzc0NzA3_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjUzMzc0NzA3_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjUzMzc0NzA3_3.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_3.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_4.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_5.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMjMzNjg5MDIz_6.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMTIzNjU4NDYz_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMTIzNjU4NDYz_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMTIzNjU4NDYz_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMDY4MzYzMDEw_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMDY4MzYzMDEw_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEzMDY4MzYzMDEw_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTQ1MzI1NTU0_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTQ1MzI1NTU0_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTQ1MzI1NTU0_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTI5MTg2OTMy_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTI5MTg2OTMy_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTI5MTg2OTMy_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTI5MTg2OTMy_3.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyOTI5MTg2OTMy_4.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyODc3NTczNDE5_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyODc3NTczNDE5_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyODc3NTczNDE5_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyODc3NTczNDE5_3.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyODc3NTczNDE5_4.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNjE0MTI1MzQ3_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNjE0MTI1MzQ3_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNjE0MTI1MzQ3_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNjE0MTI1MzQ3_3.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNjE0MTI1MzQ3_4.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTg0MjkxMTcy_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTg0MjkxMTcy_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTg0MjkxMTcy_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTg0MjkxMTcy_3.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTA4NjU2OTg3_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNTA4NjU2OTg3_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDg0OTIzODU2_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDg0OTIzODU2_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDg0OTIzODU2_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDA1MDk0NTg5_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDA1MDk0NTg5_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyNDA1MDk0NTg5_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyMTY2NjY2MzA0_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyMTY2NjY2MzA0_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEyMTY2NjY2MzA0_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNTIwNTk0NjQ2_0.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNTIwNTk0NjQ2_1.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNTIwNTk0NjQ2_2.webp",
    "http://js2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNTIwNTk0NjQ2_3.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNDk1MzM0ODI0_0.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNDk1MzM0ODI0_1.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNDk1MzM0ODI0_2.webp",
    "http://tx2.a.yximgs.com/ufile/atlas/OTUzNTE0OTMzXzEwNDk1MzM0ODI0_3.webp",
        ]
for url_l in urls:
    if url_l.startswith('https'):
        pass
    else:
        url_l = url_l.replace('http', 'https')
    print(url_l)
    # time.sleep(1)

    # hs = re.search(r'https?://(.*?)/', url_l).group(1)
    # img_id = url_l[-20:-5]
    # headerss = {
    #     "Host": hs,
    #     "Connection": "keep-alive",
    #     "Upgrade-Insecure-Requests": "1",
    #     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    #     "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    #     "Accept-Encoding": "gzip, deflate",
    #     "Accept-Language": "zh-CN,zh;q=0.9",
    #     # "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4"
    # }
    # if not os.path.exists(DATA_DIR + authorEid):
    #     os.mkdir(DATA_DIR + authorEid)
    # save_location = os.path.join(DATA_DIR, authorEid)
    #
    # file_name = os.path.join(save_location, str(img_id) + '.jpg')
    #
    # print('正在下载:' + file_name)
    #
    # res = requests.get(url=url_l, headers=headerss, proxies=real_proxy, verify=False)
    # print(res.status_code)
    # try:
    #     with open(file_name, 'wb') as f:
    #         f.write(res.content)
    #         f.close()
    #     print('已经下载...')
    # except FileNotFoundError:
    #     print('捕捉到文件名有误...')