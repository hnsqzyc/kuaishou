import requests

url = 'https://live.kuaishou.com/graphql'

header = {
    "Host": "live.kuaishou.com",
    "Connection": "keep-alive",
    "accept": "*/*",
    # "Origin": "https://live.kuaishou.com",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    # "content-type": "application/json",
    # "Referer": "https://live.kuaishou.com/profile/yuezhiyier8888",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    # "Cookie": "kuaishou.live.bfb1s=7206d814e5c089a58c910ed8bf52ace5; clientid=3; did=web_89670e2b3c892184a0452f79ea15f9b4; client_key=65890b29"
}

fm = {"operationName":"SharePageQuery","variables":{"photoId":"3xwsvci4ksxy78q","principalId":"3xheedagb6evge4"},"query":"query SharePageQuery($principalId: String, $photoId: String) {\n  feedById(principalId: $principalId, photoId: $photoId) {\n    currentWork {\n      playUrl\n      __typename\n    }\n    __typename\n  }\n}\n"}

res = requests.post(url=url, headers=header, json=fm)
print(res.text)

# con = {'data': {'feedById': {'currentWork': None, '__typename': 'ProfileFeed'}}}