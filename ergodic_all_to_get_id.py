import re
import time
import requests
from lxml import etree
from kuaishou.analyze_num import Analy

analy = Analy()

for i in range(157494640, 157494660):
    time.sleep(1)
    keyword = str(i)

    url = 'https://live.kuaishou.com/search/?keyword=%s' % (keyword)

    headers = {
        "Host": "live.kuaishou.com",
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Referer": "https://live.kuaishou.com/search/?keyword=%E4%B8%B9%E4%B8%B9",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4; didv=1557212874570; clientid=3; client_key=65890b29; needLoginToWatchHD=1; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1557212874,1557283015; __lfcc=1; kuaishou.live.bfb1s=ac5f27b3b62895859c4c1622f49856a4"
    }

    res = requests.get(url=url, headers=headers)
    # print(res.text)

    con1 = re.findall(r'相关用户', res.text)

    if con1:
        con = etree.HTML(res.text)

        user_name = con.xpath('//div[@class="profile-card-user-info-detail"]//h4//text()')[0].strip()
        print(user_name)

        fan = con.xpath('//p[@class="profile-card-user-info-counts"]/text()')[0].split()[0].replace('粉丝', '')
        fans = analy.parsingChar(fan)
        print('fans', fans)

        follow = con.xpath('//p[@class="profile-card-user-info-counts"]/text()')[0].split()[1].replace('关注', '')
        follows = analy.parsingChar(follow)
        print('follows', follows)

        photo = con.xpath('//p[@class="profile-card-user-info-counts"]/text()')[0].split()[2].replace('作品', '')
        photos = analy.parsingChar(photo)
        print('photos', photos)

        profile = con.xpath('//div[@class="profile-card-user-info-detail"]//h4/a/@href')[0].replace(r'/profile/', '')
        print('profile', profile)
        print('*' * 30)
    else:
        print(keyword)
        print('ID 为空号...')