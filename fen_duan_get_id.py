import re
import json
import requests

# 获取相关视频评论
# url = 'https://live.kuaishou.com/graphql'
#
# header = {
#     "Host": "live.kuaishou.com",
#     "Connection": "keep-alive",
#     # "Content-Length": "929",
#     "accept": "*/*",
#     "Origin": "https://live.kuaishou.com",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#     "content-type": "application/json",
#     "Referer": "https://live.kuaishou.com/profile/sisijiang666",
#     "Accept-Encoding": "gzip, deflate, br",
#     "Accept-Language": "zh-CN,zh;q=0.9",
#     # "Cookie": "clientid=3; did=web_afdd8df299ab168092a9b5aaf0a32a59; didv=1559790292634; kuaishou.live.bfb1s=477cb0011daca84b36b3a4676857e5a1; client_key=65890b29; userId=341442012; kuaishou.live.web_st=ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgActx8LEOMt2Kfw5KSF3wU4Wo-QROciv8zEQHd5FsJbDc798-YXWnMr4n8V1i6m88UN_oRNM00Y5HiZqXHEKbnmhQcaqUR4v9e-xGhoUfhd0GZ6n4mDY5CQGWKcuSAV50iGTLluIm3fUBmjP9Nl385ATK1c1ssyYjprMz32d6aCZJLOO-zdbXnDxO5Vqvg084v6BFB82zUA670D1FkbKaLWcaEgL1c1j1KEeWrOe8x-vTC5n9jyIgKuCCrg-4QVjvPeGgmyNri0aYeljIAfhZMnZwM25Bq4AoBTAB; kuaishou.live.web_ph=58fc4b1d0d52e801f6b0920e0d08f027e1c3; userId=341442012; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1559790297,1559790843; Hm_lpvt_86a27b7db2c5c0ae37fee4a8a35033ee=1559790901"
# }
#
# formdata = {"operationName":"CommentFeeds","variables":{"photoId":"3xr8wut63tz25zs","page":1,"pcursor":"","count":20},"query":"query CommentFeeds($photoId: String, $page: Int, $pcursor: String, $count: Int) {\n  shortVideoCommentList(photoId: $photoId, page: $page, pcursor: $pcursor, count: $count) {\n    commentCount\n    realCommentCount\n    pcursor\n    commentList {\n      ...BaseComment\n      subCommentCount\n      subCommentsPcursor\n      likedCount\n      liked\n      subComments {\n        commentId\n        replyToUserName\n        timestamp\n        content\n        authorName\n        authorId\n        replyTo\n        authorEid\n        headurl\n        replyToEid\n        status\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n\nfragment BaseComment on BaseComment {\n  commentId\n  authorId\n  authorName\n  content\n  headurl\n  timestamp\n  authorEid\n  status\n  __typename\n}\n"}
#
# res1 = requests.post(url=url, headers=header, json=formdata)
# res2 = res1.content.decode()
# res = json.loads(res2)
# print(res)
# next_page_pcursor = res.get('data').get('shortVideoCommentList').get('pcursor')
# print(next_page_pcursor)

# # 获取相关热门推荐
# for i in range(1):
#     url = 'https://live.kuaishou.com/graphql'
#
#     header = {
#         "Host": "live.kuaishou.com",
#         "Connection": "keep-alive",
#         #"Content-Length": "929",
#         "accept": "*/*",
#         "Origin": "https://live.kuaishou.com",
#         "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#         "content-type": "application/json",
#         "Referer": "https://live.kuaishou.com/profile/sisijiang666",
#         "Accept-Encoding": "gzip, deflate, br",
#         "Accept-Language": "zh-CN,zh;q=0.9",
#         # "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4; didv=1557212874570; clientid=3; client_key=65890b29; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1557212874,1557283015; kuaishou.live.bfb1s=9b8f70844293bed778aade6e0a8f9942; needLoginToWatchHD=1",
#         # "Cookie": "clientid=3; did=web_afdd8df299ab168092a9b5aaf0a32a59; didv=1559790292634; kuaishou.live.bfb1s=477cb0011daca84b36b3a4676857e5a1; client_key=65890b29; userId=341442012; kuaishou.live.web_st=ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgActx8LEOMt2Kfw5KSF3wU4Wo-QROciv8zEQHd5FsJbDc798-YXWnMr4n8V1i6m88UN_oRNM00Y5HiZqXHEKbnmhQcaqUR4v9e-xGhoUfhd0GZ6n4mDY5CQGWKcuSAV50iGTLluIm3fUBmjP9Nl385ATK1c1ssyYjprMz32d6aCZJLOO-zdbXnDxO5Vqvg084v6BFB82zUA670D1FkbKaLWcaEgL1c1j1KEeWrOe8x-vTC5n9jyIgKuCCrg-4QVjvPeGgmyNri0aYeljIAfhZMnZwM25Bq4AoBTAB; kuaishou.live.web_ph=58fc4b1d0d52e801f6b0920e0d08f027e1c3; userId=341442012; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1559790297,1559790843; Hm_lpvt_86a27b7db2c5c0ae37fee4a8a35033ee=1559790901"
#     }
#
#     # 注意: count:40的位置一般为20
#     formdata = {"operationName":"GetVideoRecommendFeeds","variables":{"photoId":"3xr8wut63tz25zs","count":40},"query":"query GetVideoRecommendFeeds($photoId: ID, $count: Int) {\n  videoRecommendFeeds(photoId: $photoId, count: $count) {\n    list {\n      user {\n        id\n        profile\n        name\n        __typename\n      }\n      ...VideoMainInfo\n      __typename\n    }\n    pcursor\n    __typename\n  }\n}\n\nfragment VideoMainInfo on VideoFeed {\n  photoId\n  caption\n  thumbnailUrl\n  poster\n  viewCount\n  likeCount\n  commentCount\n  timestamp\n  workType\n  type\n  playUrl\n  useVideoPlayer\n  imgUrls\n  imgSizes\n  magicFace\n  musicName\n  location\n  liked\n  onlyFollowerCanComment\n  width\n  height\n  expTag\n  __typename\n}\n"}
#
#     res = requests.post(url=url, headers=header, json=formdata)
#     print(res)
#     print(res.content.decode())

proxy = '106.59.59.227:43731'
real_proxy = {
    'http': proxy,
    'https': proxy
}

# 获取个人信息
meta = {}
meta['authorEid'] = '3xyaiz32jwph8fq'
url_1 = 'https://live.kuaishou.com/profile/{}'.format(meta['authorEid'])
header = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Cache-Control": "max-age=0",
    "Connection": "keep-alive",
    "Referer": "https://live.kuaishou.com/u/%s" % (meta['authorEid']),
    # "Cookie": "did=web_3de4395673d1b6bf02fda88c42892d58; didv=1558681788267; clientid=3; client_key=65890b29; kuaishou.live.bfb1s=3e261140b0cf7444a0ba411c6f227d88; userId=1384771623; userId=1384771623; kuaishou.live.web_st=ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgAXZvbGIZwSnjjHBbUKQ5KKPijx4Pehq9tIcoSDya-McWTS2eIqFfEe4s6aIeVcqJc-IQcaMHzHXOjwPzG4x3CtKtHiR9ji68DkwRLE1E9oS5Qsoxvz3ERYtsLlWqUPsSL5lfDfTJo02ReVXh-G9f0KubwC_0c9KvfYQqtmQSHOugGwp5wyAEoQB57BuG-Uw6vXTLngYEqwm9IaNHYCyiYS4aEiWGLfbrnUkCqTgL7M-vFrp6OSIgOgh_ENeLqXX523WMDsLrommbECy3zzzR7iWlf7VZXiYoBTAB; kuaishou.live.web_ph=379c17bcdbbe1705b56d30e4312e1ed6a75e",
    "Cookie": "kuaishou.live.bfb1s=477cb0011daca84b36b3a4676857e5a1; clientid=3; did=web_4a44ad22a5dfff734e32035bad68737a; client_key=65890b29; userId=1203909707; userId=1203909707; kuaishou.live.web_st=ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgAfB0g5JKGjq4RYl2W8USoKGUc0br6u8BqGM1fDWZlAUXjBO95dcxFg7TsTFk84KPcprJTcG4Ig-WCIw1K07w-dRDXodTMnnMY7nC3_AFYrpCrhhPBDfxPsYwm_NK5wDckln_SEgCAKdn7s0PnyUXGvMSerbmMIHFjsigYvKMVvnNeHWVcnXkMlU3fRcDSn41yQuNwY5NP3yA1Jau1C0MCMwaEqzjCHq0fUXesXMf4ciyRnDKKyIg_PbWYtq4wMt1T1bqXFw3xlpK-9xkO3kIUm7MGwj6LcMoBTAB; kuaishou.live.web_ph=02a1df556ce05f44db0d6dbd91c1b33dd9a0",
    "Host": "live.kuaishou.com",

    "Upgrade-Insecure-Requests": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36"
}

res = requests.get(url=url_1, headers=header)
print(res.text)

res1 = res.text
script = re.search(r'window.__APOLLO_STATE__ ?=([\s\S]*)}}};', res1).group(1)
# script = re.search(r'window.__APOLLO_STATE__ = (.*?);', res1).group(1)
# scripts = json.loads(script)  # 需要Cookies, 有可能获取空值导致下面解析错误
print(script)

item = {}

# 解析页面视频详细信息和下一页pcursor
principalId = re.search('\$User:(.*?)\.verifiedStatus', script).group(1)
print('principalId: ', principalId)

item['description'] = re.search('"description":"(.*?)"', script).group(1)
item['fan'] = re.search('"fan":"(.*?)"', script).group(1)
# item['fan'] = analy.parsingChar(fan)
follow = re.search('"follow":"(.*?)"', script).group(1)
# item['follow'] = analy.parsingChar(follow)
photo = re.search('"photo":"(.*?)"', script).group(1)
# item['photo'] = analy.parsingChar(photo)
item['sex'] = re.search('"sex":"(.*?)"', script).group(1)
item['authorId'] = re.search('"userId":"(.*?)"', script).group(1)
item['cityname'] = re.search('"cityName":"(.*?)"', script).group(1)

print(item)

# url_2 = 'https://live.kuaishou.com/graphql'
#
# headers = {
#     "Host": "live.kuaishou.com",
#     "Connection": "keep-alive",
#     "accept": "*/*",
#     "Origin": "https://live.kuaishou.com",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#     "content-type": "application/json",
#     # "Referer": "https://live.kuaishou.com/profile/%s" % (item['principalId']),
#     "Referer": "https://live.kuaishou.com/profile/%s" % ('yz7720101'),
#     "Accept-Encoding": "gzip, deflate, br",
#     "Accept-Language": "zh-CN,zh;q=0.9",
#     # "Cookie": "kuaishou.live.bfb1s=7206d814e5c089a58c910ed8bf52ace5; clientid=3; did=web_a6fd53cd14ed080e643e904e6f32898f; client_key=65890b29; userId=538393196; userId=538393196; kuaishou.live.web_st=ChRrdWFpc2hvdS5saXZlLndlYi5zdBKgAeEinbqt5DzPnVCu3EWdh3Uj5hXGY_MU0N2lvjVPfgasrQgkwWt-jlnJrfKHK8Tbx2bYr73rk4MEwEI1lHuS4WdkUFdk3cqI53nHFMI1hVPqJJLHRi_tN8qYUkkLXr3n1RXBMcDXSEFHIYYHhQMuyOsrPugRfCE8Fg0YYAfApjN-Jw3_DgJOXTwIuzUfZzoXVPYs8MD3IG5yJi6krqCM3_IaEqoO82cRG00nqSoI30_iGx2JSCIgTouZ6L3Wk0O0eHucgk1rnyHNUJLM7kjZKiRFEY0_xZIoBTAB; kuaishou.live.web_ph=9e4f73c6ce92f2c443ae9427fa0d97717ece"
# }
#
# formdata = {"operationName": "publicFeedsQuery",
#             "variables": {"principalId":'3xnqz7a8q7jkygs', "pcursor": "", "count": 24},
#             "query": "query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) {\n  publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) {\n    pcursor\n    live {\n      user {\n        id\n        kwaiId\n        eid\n        profile\n        name\n        living\n        __typename\n      }\n      watchingCount\n      src\n      title\n      gameId\n      gameName\n      categoryId\n      liveStreamId\n      playUrls {\n        quality\n        url\n        __typename\n      }\n      followed\n      type\n      living\n      redPack\n      liveGuess\n      anchorPointed\n      latestViewed\n      expTag\n      __typename\n    }\n    list {\n      photoId\n      caption\n      thumbnailUrl\n      poster\n      viewCount\n      likeCount\n      commentCount\n      timestamp\n      workType\n      type\n      useVideoPlayer\n      imgUrls\n      imgSizes\n      magicFace\n      musicName\n      location\n      liked\n      onlyFollowerCanComment\n      relativeHeight\n      width\n      height\n      user {\n        id\n        eid\n        name\n        profile\n        __typename\n      }\n      expTag\n      __typename\n    }\n    __typename\n  }\n}\n"}
#
# res = requests.post(url=url_2, headers=headers, json=formdata)
# con = json.loads(res.content.decode())
# print(con)
# if con['data']['publicFeeds']['pcursor'] == None:
#     print('获取视频列表的Cookie 失效 ...')
