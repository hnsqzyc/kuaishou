# -*- coding: utf8 -*-
import re
import os
import json
import yaml
import time
import logging
import requests
from urllib import parse
from jsonpath import jsonpath


def get_person_links(name, total_page):

    url1 = 'https://image.baidu.com/search/acjson?'

    for page in range(total_page):
        formdata = {
            "tn": "resultjson_com",
            "ipn": "rj",
            # "ct": "201326592",
            "is": "",
            "fp": "result",
            "queryWord": name,
            "cl": "2",
            "lm": "-1",
            "ie": "utf-8",
            "oe": "utf-8",
            "adpicid": "st  -1",
            "z": "",
            "ic": "",
            "hd": "",
            "latest": "",
            "copyright": "",
            "word": name,
            "s": "",
            "se": "",
            "tab": "",
            "width": " ",
            "height": "",
            "face": "0",
            "istype": "2",
            "qc": "",
            "nc": "1",
            "fr": "",
            "expermode": "",
            "force": "",
            "pn": page * 50,
            "rn": "50",
            "gsm": "1e",
            "1557993862214": ""
        }
        fm = parse.urlencode(formdata)

        url = url1 + fm
        header = {
            "Host": "image.baidu.com",
            "Connection": "keep-alive",
            "Accept": "text/plain, */*; q=0.01",
            "X-Requested-With": "XMLHttpRequest",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "zh-CN,zh;q=0.9"
        }

        res_1 = requests.get(url=url, headers=header).content.decode()
        # res_2 = yaml.load(res_1.replace(r'\/', '/').replace(r"'m", ''))

        photo_links = re.findall(r'thumbURL":"(.*?)"', res_1)
        for photo in photo_links:
            # photo_url = photo.get('thumbURL')
            photo_url = photo
            print(photo_url)

            if photo_url:
                photo_name = photo_url[-20:]
                print(photo_name)
                if not os.path.exists(data_dir + name):
                    os.mkdir(data_dir + name)
                save_location = os.path.join(data_dir, name)

                file_name = os.path.join(save_location, photo_name)

                logging.info('正在下载:' + file_name)
                download_image(photo_url, file_name)


def download_image(photo_url, file_name):

    header = {
        "Connection": "keep-alive",
        "Cache-Control": "max-age=0",
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
        "Accept": "*/*"
    }

    res = requests.get(url=photo_url, headers=header).content

    try:
        with open(file_name, 'wb') as f:
            f.write(res)
            f.close()
        logging.info('已经下载...')
    except FileNotFoundError:
        logging.warning('捕捉到文件名有误...')


if __name__ == '__main__':

    # 下载姓名
    name = "韩雪"

    # 下载页数, 每页50张图片
    total_page = 2

    # 保存路径
    data_dir = r'C:\Users\Administrator\Desktop\weibo\\'

    get_person_links(name, total_page)

