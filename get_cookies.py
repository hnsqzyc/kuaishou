import time
import requests

proxy = '113.120.39.4:4242'
real_proxy = {
    'http': proxy,
    'https': proxy
}

s = requests.session()


url = 'https://live.kuaishou.com/'

headers = {
    "Host": "live.kuaishou.com",
    "Connection": "keep-alive",
    "Upgrade-Insecure-s": "1",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9"
}

time.sleep(1.5)
res = s.get(url=url, headers=headers)
# print(res.cookies)

cookie_li = []
for k, v in res.cookies.items():
    kv = k + '=' + v
    cookie_li.append(kv)

cookies = '; '.join(cookie_li)
print(cookies)

# url = 'https://static.yximgs.com/s1/js/modern/app.3f076d3ec7f2e2dca686.js'
# headerr = {
#     "Host": "static.yximgs.com",
#     "Connection": "keep-alive",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#     "Accept": "*/*",
#     "Referer": "https://live.kuaishou.com/",
#     "Accept-Encoding": "gzip, deflate, br",
#     "Accept-Language": "zh-CN,zh;q=0.9"
# }
#
# conn = s.get(url=url, headers=headerr)
# print('conn', conn.text)
#
#
# url = 'https://live.kuaishou.com/rest/wd/live/web/security?detectionPoint=%7B%22a%22%3A%22Shockwave%20Flash%22%2C%22b%22%3Atrue%2C%22c%22%3Atrue%2C%22d%22%3Atrue%2C%22e%22%3Afalse%2C%22f%22%3Afalse%2C%22g%22%3Afalse%2C%22h%22%3Atrue%2C%22i%22%3Atrue%2C%22j%22%3Afalse%2C%22k%22%3Afalse%2C%22l%22%3Atrue%2C%22m%22%3Atrue%2C%22n%22%3Atrue%2C%22o%22%3Afalse%2C%22p%22%3Afalse%2C%22q%22%3Atrue%2C%22r%22%3Afalse%2C%22s%22%3Afalse%2C%22t%22%3Afalse%2C%22u%22%3Afalse%2C%22v%22%3Afalse%2C%22w%22%3Afalse%2C%22x%22%3Afalse%2C%22y%22%3Afalse%2C%22z%22%3Atrue%2C%22aa%22%3Afalse%2C%22bb%22%3Afalse%2C%22cc%22%3Afalse%2C%22dd%22%3Afalse%2C%22ee%22%3Afalse%2C%22ff%22%3Afalse%2C%22gg%22%3Afalse%2C%22hh%22%3Afalse%2C%22ii%22%3Afalse%2C%22jj%22%3Afalse%2C%22kk%22%3Afalse%2C%22ll%22%3Afalse%2C%22mm%22%3Afalse%2C%22nn%22%3Afalse%2C%22oo%22%3Afalse%2C%22pp%22%3A1%7D&collectInfo=%7B%22a%22%3A%22live.kuaishou.com%22%2C%22b%22%3A%22https%3A%2F%2Flive.kuaishou.com%2F%22%2C%22c%22%3A%22%E9%A6%96%E9%A1%B5-%E5%BF%AB%E6%89%8B%E7%9B%B4%E6%92%AD%22%2C%22d%22%3A%22%22%2C%22e%22%3A%22Mozilla%2F5.0%20(Windows%20NT%2010.0%3B%20WOW64)%20AppleWebKit%2F537.36%20(KHTML%2C%20like%20Gecko)%20Chrome%2F72.0.3626.121%20Safari%2F537.36%22%2C%22f%22%3A1080%2C%22g%22%3A1920%2C%22h%22%3A24%2C%22i%22%3A%22zh-CN%22%7D'
# header = {
#     "Host": "live.kuaishou.com",
#     "Connection": "keep-alive",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#     "Accept": "image/webp,image/apng,image/*,*/*;q=0.8",
#     "Referer": "https://live.kuaishou.com/",
#     "Accept-Encoding": "gzip, deflate, br",
#     "Accept-Language": "zh-CN,zh;q=0.9",
#     # "Cookie": cookies
# }
# res = s.get(url, headers=header)
# print(res.text)
#
# url1 = 'https://id.kuaishou.com/pass/kuaishou/login/passToken'
# header = {
#     "Host": "id.kuaishou.com",
#     "Connection": "keep-alive",
#     "Content-Length": "21",
#     "Accept": "application/json, text/plain, */*",
#     "Origin": "https://live.kuaishou.com",
#     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
#     "Content-Type": "application/x-www-form-urlencoded",
#     "Referer": "https://live.kuaishou.com/",
#     "Accept-Encoding": "gzip, deflate, br",
#     "Accept-Language": "zh-CN,zh;q=0.9",
#     # "Cookie": cookies
# }
#
# formdata = {
#     'sid': 'kuaishou.live.web'
# }
#
# con = s.post(url=url1, headers=header, data=formdata)
# print(con.text)

url = 'https://live.kuaishou.com/rest/wd/live/web/collect'
headerss = {
    "Host": "live.kuaishou.com",
    "Connection": "keep-alive",
    "Content-Length": "1410",
    "Origin": "https://live.kuaishou.com",
    "kpn": "GAME_ZONE",
    "kpf": "PC_WEB",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "Content-Type": "multipart/form-data; boundary=----WebKitFormBoundaryYcbm9vvnjLRAoUmK",
    "Accept": "*/*",
    "Referer": "https://live.kuaishou.com/",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Cookie": "clientid=3; did=web_582343818f4c2c9206d2c9d9a18c19d3; client_key=65890b29; kuaishou.live.bfb1s=9b8f70844293bed778aade6e0a8f9942"
}

fm = {
    
}