import requests
import re
import json
import time
import random
from jsonpath import jsonpath
from lxml import etree
from pymongo import MongoClient
from kuaishou.analyze_num import Analy

proxy = '49.69.91.141:4276'
real_proxy = {
    'http': proxy,
    'https': proxy
}

# url = 'http://www.baidu.com/'
# con = requests.get(url=url, proxies=real_proxy)
# print('百度搜索:', con.status_code)



url = 'mongodb://47.105.103.8:27017/'
mongo_conn = MongoClient(url)
db = mongo_conn.kuaishou
ks = db.ks


analy = Analy()

# userId = '3x26ypywmrkyrzg'
# userId = '135747435' # 此处userId 无法使用

# userId = '3xhev8ryc37ak8e'

# userId = '3xh9fi7gkasvk7a'

# userId = '3xadfhj5fue2tem'

# userId = '3xrptqgjk98rism'

# userId = '3xsx74sidgkz2bq'

# userId = 'GG7979521'  # 郭芳
# userId = '3xggrctr9dvj45w'

# userId = '3x4946fsn9tnuvg'

userId = 'tiaozige'
# userId = 'mumu10520'
# userId = '157494646'
# userId = '823429166'
# userId = '3xunnapfmubhazw'
# url = 'https://live.kuaishou.com/profile/157494646'
url = 'https://live.kuaishou.com/profile/{}'.format(userId)
# url = 'https://live.kuaishou.com/profile/dandan668'
cookies = [
    # "{'cookie':{'kuaishou.live.bfb1s':'477cb0011daca84b36b3a4676857e5a1','clientid':'3','did':'web_3de4395673d1b6bf02fda88c42892d58','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'ac5f27b3b62895859c4c1622f49856a4','clientid':'3','did':'web_feffe1939475ab4606520d16a7f78de4','client_key':'65890b29'}}",
    # "{'cookie':{'clientid':'3','did':'web_8827d89f9562a11b7fddc1254f56ed32','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'ac5f27b3b62895859c4c1622f49856a4','clientid':'3','did':'web_437004cf5222f2d3da78d2b80f557f38','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'7206d814e5c089a58c910ed8bf52ace5','clientid':'3','did':'web_bf1db9a05aca76685174bad7bbc0f26b','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'ac5f27b3b62895859c4c1622f49856a4','clientid':'3','did':'web_1ca7d7436f8413f5d29ad1f338dfffa0','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'ac5f27b3b62895859c4c1622f49856a4','clientid':'3','did':'web_a549f57b8d9fcc8a115a8d259575b030','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'ac5f27b3b62895859c4c1622f49856a4','clientid':'3','did':'web_a6b9b74f58e4cc47c03a9eb0fc4ad9e2','client_key':'65890b29'}}",
    # "{'cookie':{'kuaishou.live.bfb1s':'7206d814e5c089a58c910ed8bf52ace5','clientid':'3','did':'web_3ef18e5f4198112d8d0fdabbdc0edaa9','client_key':'65890b29'}}",
    # "did=web_3de4395673d1b6bf02fda88c42892d58; didv=1558681788267; clientid=3; client_key=65890b29; kuaishou.live.bfb1s=3e261140b0cf7444a0ba411c6f227d88",
    # "clientid=3; did=web_2fee652a6fcd0a3c9ad9446dbfa94561; client_key=65890b29",
    # "clientid=3; did=web_29dbc177883b8d740c482b77c207f385; client_key=65890b29",
    # "clientid=3; did=web_2c0f692499f2998ad92ee254719dc68d; client_key=65890b29",
    # "clientid=3; did=web_94cf44f3bba33eb864936bed8717596c; client_key=65890b29",
    # "clientid=3; did=web_f7c403d7e8e2fd23b4ed5439b5a1e108; client_key=65890b29",
    # "clientid=3; did=web_c15c93e6875dd8f7f1bc0d3b91971399; client_key=65890b29",
    # "clientid=3; did=web_8827d89f9562a11b7fddc1254f56ed32; client_key=65890b29",
    "clientid=3; did=web_d4a31a9c9c2c06ccab75d96ad505edfa; client_key=65890b29",
]

hd = [
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
    "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
    "Opera/9.80 (Macintosh; Intel Mac OS X 10.6.8; U; en) Presto/2.8.131 Version/11.11",
    "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11",
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Maxthon 2.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; TencentTraveler 4.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SE 2.X MetaSr 1.0; SE 2.X MetaSr 1.0; .NET CLR 2.0.50727; SE 2.X MetaSr 1.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; 360SE)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Avant Browser)",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36 OPR/26.0.1656.60 Opera/8.0 (Windows NT 5.1; U; en)",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:34.0) Gecko/20100101 Firefox/34.0",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36",
    "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.133 Safari/534.16",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER",
    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729)",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)",
    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.84 Safari/535.11 SE 2.X MetaSr 1.0",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; SE 2.X MetaSr 1.0)",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.122 UBrowser/4.0.3214.0 Safari/537.36",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; SE 2.X MetaSr 1.0)"
    "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko"
]

for hi in hd:
    time.sleep(3)
    header = {
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "zh-CN,zh;q=0.9",
        "Cache-Control": "max-age=0",
        "Connection": "keep-alive",
        # "Referer": "https://live.kuaishou.com/u/3xhev8ryc37ak8e",
        # "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4; didv=1557212874570; clientid=3; client_key=65890b29; needLoginToWatchHD=1; kuaishou.live.bfb1s=9b8f70844293bed778aade6e0a8f9942; __lfcc=1; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1557212874,1557283015,1557829749; Hm_lpvt_86a27b7db2c5c0ae37fee4a8a35033ee=1557829749",
        "Cookie": random.choice(cookies),
        "Host": "live.kuaishou.com",
        "Upgrade-Insecure-Requests": "1",
        "User-Agent": hi
    }
    print(header['Cookie'])
    res1 = requests.get(url, headers=header)
    # print(res1.text)
    # res = etree.HTML(res1.text)
    # username = res.xpath('//div[@class="user-info"]//p[@class="user-info-name"]/text()')[0].strip()
    # print('username', username)
    # userinfo = res.xpath('//div[@class="user-info"]//p[@class="user-info-other"]/span/text()')
    # print('userinfo', userinfo)
    # userdes = res.xpath('//div[@class="user-info"]//p[@class="user-info-description"]/text()')[0]
    # print('userdes', userdes)
    # fans = res.xpath('//div[@class="user-data-item fans"]/text()')[0]
    # print('fans', analy.parsingChar(fans))
    # follow = res.xpath('//div[@class="user-data-item follow"]/text()')[0]
    # print('follow', analy.parsingChar(follow))
    # work = res.xpath('//div[@class="user-data-item work"]/text()')[0]
    # print('work', analy.parsingChar(work))
    try:
        item = {}
        # print(res1.text)
        # script = re.search(r'window.__APOLLO_STATE__ = (.*?)"__typename":"VideoFeed"}}};', res1.text).group()
        script = re.search(r'window.__APOLLO_STATE__ =([\s\S]*)}}};', res1.text).group(1)
        # scripts = json.loads(script)  # 需要Cookies, 有可能获取空值导致下面解析错误
        # print(scripts)

        # 解析页面视频详细信息和下一页pcursor
        print(script)
        principalId = re.search('\$User:(.*?)\.verifiedStatus', script).group(1)
        print('principalId: ', principalId)
        item['principalId'] = principalId

        name = re.search('"name":"(.*?)"', script).group(1)
        print('name: ', name)
        eid = re.search('"eid":"(.*?)"', script).group(1)
        print('eid: ', eid)

        item['description'] = re.search('"description":"(.*?)"', script).group(1)
        print('description: ', item['description'])
        fan = re.search('"fan":"(.*?)"', script).group(1)
        item['fan'] = analy.parsingChar(fan)
        print('fan: ', item['fan'])
        follow = re.search('"follow":"(.*?)"', script).group(1)
        item['follow'] = analy.parsingChar(follow)
        print('follow: ', item['follow'])
        photo = re.search('"photo":"(.*?)"', script).group(1)
        item['photo'] = analy.parsingChar(photo)
        print('photo: ', item['photo'])
        item['sex'] = re.search('"sex":"(.*?)"', script).group(1)
        print('sex: ', item['sex'])
        item['authorId'] = re.search('"userId":"(.*?)"', script).group(1)
        print('authorId: ', item['authorId'])
        item['cityName'] = re.search('"cityName":"(.*?)"', script).group(1)
        print('cityName: ', item['cityName'])
        print('%s 可用...' % (hi))
    except:
        print('%s 不可用...' % (hi))







# userinfo = jsonpath(scripts, '$..defaultClient.User:{}'.format(principalId))[0]
# key = '$User:{}.countsInfo'.format(principalId)
# usercountsinfo = scripts.get('defaultClient').get(key)
# # print('userinfo', userinfo)
# username = userinfo.get('name')
# print('username', username)
# eid = userinfo.get('eid')
# print('eid', eid)
#
# item['description'] = userinfo.get('description')
# print('description', item['description'])
# # ks.update({'authorEid': userId}, {'$set': {'description': description}})
#
# item['fan'] = analy.parsingChar(usercountsinfo.get('fan'))
# print('fan', item['fan'])
# # ks.update({'authorEid': userId}, {'$set': {'fan': fan}})
#
# item['follow'] = analy.parsingChar(usercountsinfo.get('follow'))
# print('follow', item['follow'])
# # ks.update({'authorEid': userId}, {'$set': {'follow': follow}})
#
# item['photo'] = analy.parsingChar(usercountsinfo.get('photo'))
# print('photo', item['photo']) # 作品数量
# # ks.update({'authorEid': userId}, {'$set': {'photo': photo}})
#
# item['sex'] = userinfo.get('sex')
# print('sex', item['sex'])
# item['cityname'] = userinfo.get('cityName')
# print('cityname', item['cityname'])
#
# item['authorId'] = userinfo.get('userId')
# print('authorId: ', item['authorId'])
# # ks.update({'authorEid': userId}, {'$set': {'authorId': userid}})
#
# ks.update({'authorEid': userId}, {'$set': item})
