import re
import json
import requests
from jsonpath import jsonpath

url = 'https://live.kuaishou.com/graphql'

headers = {
    "Host": "live.kuaishou.com",
    "Connection": "keep-alive",
    "accept": "*/*",
    "Origin": "https://live.kuaishou.com",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
    "content-type": "application/json",
    "Referer": "https://live.kuaishou.com/profile/CDDhehehe",
    "Accept-Encoding": "gzip, deflate, br",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4; kuaishou.live.bfb1s=9b8f70844293bed778aade6e0a8f9942"
    # "Cookie": "did=web_08b65cdcf1a001036c9c9df32799e545"
}

# formdata = {"operationName":"publicFeedsQuery","variables":{"principalId":"3xsx74sidgkz2bq","pcursor":"","count":24},"query":"query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) {\n  publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) {\n    pcursor\n    live {\n      user {\n        id\n        kwaiId\n        eid\n        profile\n        name\n        living\n        __typename\n      }\n      watchingCount\n      src\n      title\n      gameId\n      gameName\n      categoryId\n      liveStreamId\n      playUrls {\n        quality\n        url\n        __typename\n      }\n      followed\n      type\n      living\n      redPack\n      liveGuess\n      anchorPointed\n      latestViewed\n      expTag\n      __typename\n    }\n    list {\n      photoId\n      caption\n      thumbnailUrl\n      poster\n      viewCount\n      likeCount\n      commentCount\n      timestamp\n      workType\n      type\n      useVideoPlayer\n      imgUrls\n      imgSizes\n      magicFace\n      musicName\n      location\n      liked\n      onlyFollowerCanComment\n      relativeHeight\n      width\n      height\n      user {\n        id\n        eid\n        name\n        profile\n        __typename\n      }\n      expTag\n      __typename\n    }\n    __typename\n  }\n}\n"}
formdata = {"operationName":"publicFeedsQuery","variables":{"principalId":"sisijiang666","pcursor":"","count":24},"query":"query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) {\n  publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) {\n    pcursor\n    live {\n      user {\n        id\n        kwaiId\n        eid\n        profile\n        name\n        living\n        __typename\n      }\n      watchingCount\n      src\n      title\n      gameId\n      gameName\n      categoryId\n      liveStreamId\n      playUrls {\n        quality\n        url\n        __typename\n      }\n      followed\n      type\n      living\n      redPack\n      liveGuess\n      anchorPointed\n      latestViewed\n      expTag\n      __typename\n    }\n    list {\n      photoId\n      caption\n      thumbnailUrl\n      poster\n      viewCount\n      likeCount\n      commentCount\n      timestamp\n      workType\n      type\n      useVideoPlayer\n      imgUrls\n      imgSizes\n      magicFace\n      musicName\n      location\n      liked\n      onlyFollowerCanComment\n      relativeHeight\n      width\n      height\n      user {\n        id\n        eid\n        name\n        profile\n        __typename\n      }\n      expTag\n      __typename\n    }\n    __typename\n  }\n}\n"}

res1 = requests.post(url=url, headers=headers, json=formdata).content.decode()
res = json.loads(res1)
print(res)
info_list = res['data']['publicFeeds']['list']
for info in info_list:
    photoId = info.get('photoId')
    viewCount = info.get('viewCount')
    likeCount = info.get('likeCount')
    workType = info.get('workType')
    imgUrls = info.get('imgUrls')
    timestamp = info.get('timestamp')
    caption = info.get('caption')
    print(photoId, viewCount, likeCount, workType, imgUrls, timestamp, caption)

