import time
import json
import redis
import requests
from selenium import webdriver


# r =redis.Redis(host="47.105.103.8", port=56789, password="12345678")
# r =redis.Redis(host="192.168.1.28", port=6379)
r =redis.Redis(host="127.0.0.1", port=6379)


def get_new_ip():

    url = 'http://webapi.http.zhimacangku.com/getip?num=1&type=2&pro=&city=0&yys=0&port=11&time=2&ts=0&ys=0&cs=0&lb=1&sb=0&pb=45&mr=1&regions='
    res1 = requests.get(url=url).text
    res = json.loads(res1)
    print(res)

    ip = res['data'][0]['ip']
    port = res['data'][0]['port']

    con = '{}:{}'.format(ip, port)
    print('请求结果: ', con)
    return con


def get_ip():

    proxy = r.rpoplpush('mobile_ua', 'mobile_ua').decode()
    print(proxy)

    return proxy


def verify_ip():

    proxy = get_new_ip()

    real_proxy = {
        'http': proxy,
        'https': proxy
    }
    url = 'http://www.baidu.com/'
    res = requests.get(url, proxies=real_proxy)
    if res.status_code == 200:
        return proxy
    else:
        return False


def get_webpage():

    # pro = verify_ip()
    pro = 1

    if pro:
        options = webdriver.ChromeOptions()
        options.binary_location = r"D:\software_location\cent_install\CentBrowser\Application\chrome.exe"
        # options.binary_location = '/usr/bin/google-chrome-stable'

        # # Linux 环境下配置
        # options.add_argument('--headless')
        # options.add_argument('--no-sandbox')
        # options.add_argument('--disable-extensions')
        # options.add_argument('--disable-gpu')

        # options.add_argument("--proxy-server=http://%s" % (pro))
        # options.add_argument("--proxy-server=http://%s" % ('114.237.50.196:4256'))

        # driver = webdriver.Chrome(executable_path='/home/seeta/zhangyanchao/chromedriver_install/chromedriver',
        #                           chrome_options=options)
        driver = webdriver.Chrome(executable_path='D:\projects\Weibo_projects\chromedriver.exe',
                                  chrome_options=options)

        # # 设置代理
        # options.add_argument("--proxy-server=http://%s" % (pro))
        # options.add_argument("--proxy-server=http://%s" % ('119.129.236.251:4206'))

        # # 使用 PhantomJS 初始化
        # service_args = ['--proxy=119.129.236.251:4206', '--proxy-type=socks5', ]
        # driver = webdriver.PhantomJS(r'C:\Users\Administrator\Desktop\phantomjs-2.1.1-windows\phantomjs-2.1.1-windows\bin\phantomjs.exe', service_args=service_args)

        # # 一定要注意，=两边不能有空格，不能是这样--proxy-server = http://202.20.16.82:10152
        # driver = webdriver.Chrome(chrome_options=options)
        # driver.maximize_window()

        # 查看本机ip，查看代理是否起作用
        driver.get("http://httpbin.org/ip")
        print(driver.page_source)

        # driver.get('https://live.kuaishou.com/')
        # cookies = driver.get_cookies()
        # print(cookies)

        # cookie = {}
        # for res in cookies:
        #     name = res['name']
        #     value = res['value']
        #     # print(res['name'], res['value'])
        #     cookie[name] = value
        # cookie_d = '"' + "{'cookie':" + str(cookie) + '}"'  # 插入后还有双引号, 可能影响结果
        # print(cookie_d)

        # 上传到服务器
        # r.lpush('ks_cookies', cookie_d)
        cookiess = {'kuaishou.live.bfb1s': '3e261140b0cf7444a0ba411c6f227d88', 'did': 'web_feffe1939475ab4606520d16a7f78de4', 'client_key': '65890b29', 'clientid': '3'}
        for k, v in cookiess.items():
            driver.add_cookie({
                'domain': '.kuaishou.com',
                'name': k,
                'value': v,
                'path': '/',
                'expires': '1590824726.052415'

            })
        driver.get('https://live.kuaishou.com/profile/dagaoge666')

        for i in range(2):  # 测试发现, 至少 refresh 一次
            time.sleep(1.5)
            driver.refresh()

        # 退出，清除浏览器缓存
        time.sleep(10)
        driver.quit()


if __name__ == '__main__':
    get_webpage()