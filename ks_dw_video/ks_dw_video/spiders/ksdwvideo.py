# -*- coding: utf-8 -*-
import scrapy
import json
import re
import os
import time
import random
import logging
from scrapy import signals
from scrapy.item import Item, Field
from scrapy.http import Request, FormRequest
from scrapy.utils.project import get_project_settings
from ks_dw_video.connection import RedisConnection, MongodbConnection

settings = get_project_settings()


class UniversalRow(Item):
    # This is a row wrapper. The key is row and the value is a dict
    # The dict wraps key-values of all fields and their values
    row = Field()
    table = Field()
    image_urls = Field()


class KsdwvideoSpider(scrapy.Spider):
    name = 'ksdwvideo'

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(KsdwvideoSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_opened, signals.spider_opened)
        crawler.signals.connect(spider.spider_closed, signals.spider_closed)

        return spider

    def __init__(self, params, *args, **kwargs):

        super(KsdwvideoSpider, self).__init__(self.name, *args, **kwargs)
        # dispatcher.connect(self.spider_closed, signals.spider_closed)
        paramsjson = json.loads(params)
        self.remote_resource = paramsjson.get('remote_resource', True)
        self.enable_proxy = paramsjson.get('enable_proxy', True)

    def spider_opened(self, spider):
        logging.info("爬取开始了...")

        self.redis_conn = RedisConnection(settings['REDIS']).get_conn()
        self.mongo_conn = MongodbConnection(settings['MONGODB']).get_conn()
        self.db = self.mongo_conn.kuaishou
        self.ks = self.db.ks
        self.ks_video = self.db.ks_video

    def spider_closed(self, spider):

        logging.info('爬取结束了..')

    def start_requests(self):
        while 1:
            result = self.ks_video.find({'dw_url': 0}, {'_id': 0, 'authorEid': 1, 'photoId': 1}).limit(1)  # 修改为dw_url
            if result.count():
                for res in result:
                    meta = {}
                    meta['authorEid'] = res['authorEid']
                    meta['photoId'] = res['photoId']

                    # 休眠不定长时间
                    time.sleep(random.random())

                    # 如果发现此时的authorEid已经改变状态, 就略过本次操作
                    yuguo = self.ks_video.find({'photoId': meta['photoId']}, {'_id': 0, 'dw_url': 1})[0]['dw_url']
                    if yuguo:
                        pass
                    else:
                        logging.info('拿到以后不用等下载图片数量及链接,马上把状态修改了...')
                        ti = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw_url': 1}})

                        # 获取相关视频评论
                        url = 'https://live.kuaishou.com/graphql'

                        header = {
                            "Host": "live.kuaishou.com",
                            "Connection": "keep-alive",
                            "accept": "*/*",
                            "Origin": "https://live.kuaishou.com",
                            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                            "content-type": "application/json",
                            # "Referer": "https://live.kuaishou.com/profile/sisijiang666",  # 如果没有, 后期很容易发生错误
                            "Accept-Encoding": "gzip, deflate, br",
                            "Accept-Language": "zh-CN,zh;q=0.9",
                            # "Cookie": "did=web_89670e2b3c892184a0452f79ea15f9b4; didv=1557212874570; clientid=3; client_key=65890b29; needLoginToWatchHD=1; kuaishou.live.bfb1s=7206d814e5c089a58c910ed8bf52ace5; Hm_lvt_86a27b7db2c5c0ae37fee4a8a35033ee=1557919245,1557990253,1558063211,1558065519; Hm_lpvt_86a27b7db2c5c0ae37fee4a8a35033ee=1558065519"
                        }

                        fm_get_video_url = {"operationName": "SharePageQuery",
                                            "variables": {"photoId": meta['photoId'], "principalId": meta['authorEid']},
                                            "query": "query SharePageQuery($principalId: String, $photoId: String) {\n  feedById(principalId: $principalId, photoId: $photoId) {\n    currentWork {\n      playUrl\n      __typename\n    }\n    __typename\n  }\n}\n"}

                        # meta = {}
                        meta['url'] = url
                        meta['header'] = header
                        meta['fm_get_video_url'] = fm_get_video_url
                        meta['page'] = 1
                        meta['request_num'] = 1

                        # TODO 添加敏感词, 判断是否下载视频
                        hugo = self.ks.find({'authorEid': meta['authorEid']}, {'_id': 0, 'sensitive': 1, 'photo': 1})

                        if not hugo[0]['sensitive'] and 7 <= int(hugo[0].get('photo')) <= 1500:  # 没有敏感词并且视频数量在阈值范围内
                            # 判断是否为图片并下载
                            con = self.ks_video.find({'photoId': meta['photoId']}, {'_id': 0, 'imgUrls': 1, 'workType': 1})
                            for link in con:
                                if link.get('workType') != 'video' and link.get('imgUrls'):

                                    # TODO  判断为图片以后 修改下载状态为1
                                    sup = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw_url': 1, 'dw': 1}})

                                    for url_l in link.get('imgUrls'):
                                        if url_l.startswith('https'):  # 处理因为使用代理返回404的错误
                                            pass
                                        else:
                                            url_l = url_l.replace('http', 'https')

                                        # TODO 下载图片位置
                                        hs = re.search(r'https?://(.*?)/', url_l).group(1)
                                        img_id = url_l[-20:-5]
                                        headerss = {
                                            "Host": hs,
                                            "Connection": "keep-alive",
                                            "Upgrade-Insecure-Requests": "1",
                                            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                                            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                                            # "Referer": "https://live.kuaishou.com/profile/{}".format(meta['authorEid']),
                                            "Accept-Encoding": "gzip, deflate",
                                            "Accept-Language": "zh-CN,zh;q=0.9"
                                        }

                                        try:
                                            if not os.path.exists(settings['DATA_DIR'] + meta['authorEid']):
                                                os.mkdir(settings['DATA_DIR'] + meta['authorEid'])
                                            save_location = os.path.join(settings['DATA_DIR'], meta['authorEid'])

                                            file_name = os.path.join(save_location, str(img_id) + '.jpg')

                                            if os.path.exists(file_name):
                                                print('文件已经存在...')
                                                logging.warning('文件已经存在...')
                                            else:
                                                meta['file_name'] = file_name
                                                logging.info(meta['file_name'])
                                                logging.info('正在下载:' + file_name)
                                                yield Request(url=url_l, headers=headerss, callback=self.download_image, meta=meta,
                                                              priority=10, dont_filter=True)
                                        except FileExistsError:
                                            pass

                                elif link.get('workType') != 'video' and not link.get('imgUrls'):
                                    # 类型不是video,但是没有图片链接, 把 dw_url 状态修改为3
                                    self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw_url': 3, 'dw': 3}})

                                elif link.get('workType') == 'video':
                                    print('下载视频链接...')
                                    logging.info('下载视频链接...')

                                    # 获取视频下载地址 # TODO　即使链接存在也可能被删除掉了, 状态码404
                                    yield Request(method='POST', url=url, headers=header, body=json.dumps(fm_get_video_url),
                                                  callback=self.parse_video_url,
                                                  meta=meta, dont_filter=True)
                        else:
                            print('个人信息含有敏感词 或 视频数量不合格 不予下载...')
                            logging.warning('个人信息含有敏感词 或 视频数量不合格 不予下载...')

                            # 把不合格的id做批量修改
                            res1 = self.ks_video.update({'authorEid': meta['authorEid']}, {'$set': {'dw_url': 5, 'dw': 5}}, multi=True, upsert=False)
            else:
                print('dw_url 状态为0的已经没有了...')
                logging.warning('dw_url 状态为0的已经没有了...')
                return

    def parse_video_url(self, response):

        # if not self.verify_response(response):
        #     return response

        meta = response.request.meta

        if not json.loads(response.text)['data']['feedById']['currentWork']:
            print('修改可能不存在视频状态为4')
            resul = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw_url': 4, 'dw': 4}})

        elif 'errors' in response.text:
            print('用户可能被封禁, 修改状态为10, 批量修改...')
            difo = self.ks_video.update({'authorEid': meta['authorEid']}, {'$set': {'dw_url': 10, 'dw': 10, 'dw_relative': 10}}, multi=True, upsert=False)

        else:
            con = json.loads(response.text)
            video_url = con.get('data').get('feedById').get('currentWork').get('playUrl')

            # 视频链接插入数据库
            resul = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'video_url': video_url}})

            # # 是否已经下载视频链接***** IP 问题单独使用程序下载链接
            resu = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw_url': 1}})

            # TODO 下载视频位置
            hs = re.search(r'https://(.*?)/', video_url).group(1)
            img_id = video_url[-20:]
            header = {
                "Host": hs,
                "Connection": "keep-alive",
                "Upgrade-Insecure-Requests": "1",
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                "Accept-Encoding": "gzip, deflate, br",
                "Accept-Language": "zh-CN,zh;q=0.9"
            }

            if not os.path.exists(settings['DATA_DIR'] + meta['authorEid']):
                os.mkdir(settings['DATA_DIR'] + meta['authorEid'])
            save_location = os.path.join(settings['DATA_DIR'], meta['authorEid'])

            file_name = os.path.join(save_location, str(img_id))

            if os.path.exists(file_name):
                print('文件已经存在...')
                logging.warning('文件已经存在...')
            else:
                meta['file_name'] = file_name
                logging.info(meta['file_name'])
                logging.info('正在下载:' + file_name)

                # TODO　即使链接存在也可能被删除掉了, 状态码404
                yield Request(url=video_url, headers=header, callback=self.download_image, meta=meta, priority=10)

                # 是否已经下载视频
                res = self.ks_video.update({'photoId': meta['photoId']}, {'$set': {'dw': 1}})

    def download_image(self, response):
        meta = response.request.meta
        res = response.body
        if response.status == 404:
            pass
        else:
            try:
                with open(meta['file_name'], 'wb') as f:
                    f.write(res)
                    # f.close()
                logging.info('已经下载...')
            except FileNotFoundError:
                logging.warning('捕捉到文件名有误...')

    def verify_response(self, response):
        if not response.body:  # In case response is empty
            logging.warn('Response is empty: %s' % (response.url))
            return False
        if response.status != 200:
            logging.warning('Response status: %d', response.status)
            logging.warning('Response body: %s' % response.body.decode())

        # verify response result
        result = json.loads(response.body)
        if 'errors' in result:
            return False

        return True
