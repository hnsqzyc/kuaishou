# -*- coding: utf-8 -*-
import scrapy
import json
import re
import os
import yaml
import logging
from scrapy import signals
from jsonpath import jsonpath
from scrapy.item import Item, Field
from scrapy.http import Request, FormRequest
from scrapy.utils.project import get_project_settings
from ks_get_eid.analyze_num import Analy
from ks_get_eid.connection import RedisConnection, MongodbConnection

import sys
import codecs
sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())


settings = get_project_settings()

analy = Analy()


class UniversalRow(Item):
    # This is a row wrapper. The key is row and the value is a dict
    # The dict wraps key-values of all fields and their values
    row = Field()
    table = Field()
    image_urls = Field()


class KsGeteidSpider(scrapy.Spider):
    name = 'ks_getEid'

    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super(KsGeteidSpider, cls).from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_opened, signals.spider_opened)
        crawler.signals.connect(spider.spider_closed, signals.spider_closed)

        return spider

    def __init__(self, params, *args, **kwargs):

        super(KsGeteidSpider, self).__init__(self.name, *args, **kwargs)
        # dispatcher.connect(self.spider_closed, signals.spider_closed)
        paramsjson = json.loads(params)
        self.remote_resource = paramsjson.get('remote_resource', True)
        self.enable_proxy = paramsjson.get('enable_proxy', True)

    def spider_opened(self, spider):
        logging.info("爬取开始了...")

        self.redis_conn = RedisConnection(settings['REDIS']).get_conn()
        self.mongo_conn = MongodbConnection(settings['MONGODB']).get_conn()
        self.db = self.mongo_conn.kuaishou
        self.ks = self.db.ks
        self.ks_video = self.db.ks_video

    def spider_closed(self, spider):

        logging.info('爬取结束了..')

    def start_requests(self):

        # userId = '3xf48bj5ykmxzrm'

        # userId = '3xggrctr9dvj45w' # 隐私用户 详细信息可以解析
        # while 1:

        result = self.ks.find({'status': 0}, {'_id': 0, 'authorEid': 1, 'authorName': 1}).limit(5000)
        if result.count():
            for res in result:
                meta = {}
                meta['authorEid'] = res['authorEid']
                meta['authorName'] = res['authorName']
                # meta['authorEid'] = '3xf48bj5ykmxzrm'

                url_1 = 'https://live.kuaishou.com/profile/{}'.format(meta['authorEid'])
                header = {
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "zh-CN,zh;q=0.9",
                    "Cache-Control": "max-age=0",
                    "Connection": "keep-alive",
                    "Referer": "https://live.kuaishou.com/u/%s" % (meta['authorEid']),
                    # "Cookie": "clientid=3; did=web_16e0d1da520c12d438630829c7edec25; client_key=65890b29; kuaishou.live.bfb1s=3e261140b0cf7444a0ba411c6f227d88",
                    "Host": "live.kuaishou.com",
                    "Upgrade-Insecure-Requests": "1",
                    "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
                }

                # url_2 = 'https://live.kuaishou.com/graphql'
                #
                # headers = {
                #     "Host": "live.kuaishou.com",
                #     "Connection": "keep-alive",
                #     "accept": "*/*",
                #     "Origin": "https://live.kuaishou.com",
                #     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                #     "content-type": "application/json",
                #     # "Referer": "https://live.kuaishou.com/profile/CDDhehehe",
                #     "Accept-Encoding": "gzip, deflate, br",
                #     "Accept-Language": "zh-CN,zh;q=0.9",
                #     # "Cookie": "clientid=3; did=web_dc309322be1e34669a8dbdb93483950a; client_key=65890b29; kuaishou.live.bfb1s=3e261140b0cf7444a0ba411c6f227d88"
                # }
                #
                # formdata = {"operationName": "publicFeedsQuery",
                #             "variables": {"principalId": meta['authorEid'], "pcursor": "", "count": 24},
                #             "query": "query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) {\n  publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) {\n    pcursor\n    live {\n      user {\n        id\n        kwaiId\n        eid\n        profile\n        name\n        living\n        __typename\n      }\n      watchingCount\n      src\n      title\n      gameId\n      gameName\n      categoryId\n      liveStreamId\n      playUrls {\n        quality\n        url\n        __typename\n      }\n      followed\n      type\n      living\n      redPack\n      liveGuess\n      anchorPointed\n      latestViewed\n      expTag\n      __typename\n    }\n    list {\n      photoId\n      caption\n      thumbnailUrl\n      poster\n      viewCount\n      likeCount\n      commentCount\n      timestamp\n      workType\n      type\n      useVideoPlayer\n      imgUrls\n      imgSizes\n      magicFace\n      musicName\n      location\n      liked\n      onlyFollowerCanComment\n      relativeHeight\n      width\n      height\n      user {\n        id\n        eid\n        name\n        profile\n        __typename\n      }\n      expTag\n      __typename\n    }\n    __typename\n  }\n}\n"}

                # meta = {}
                meta['url_1'] = url_1
                # meta['url_2'] = url_2
                meta['header'] = header
                # meta['headers'] = headers
                meta['page'] = 1
                meta['request_num'] = 1
                # meta['formdata'] = formdata

                # print('拿到以后不用等下载详细信息及视频列表,马上把状态修改了...')
                logging.info('拿到以后不用等下载详细信息及视频列表,马上把状态修改了...')
                result = self.ks.update({'authorEid': meta['authorEid']}, {'$set': {'status': 1}})

                yield Request(url=url_1, headers=header, callback=self.parse_profile, # Cookie 失效 principalId null
                              meta=meta, dont_filter=True, priority=10)

                # yield Request(method='POST', url=url_2, headers=headers, body=json.dumps(formdata), callback=self.parse_video_list,
                #               meta=meta, dont_filter=True) # Cookie 失效 {'data': {'publicFeeds': {'pcursor': None, 'live': None, 'list': [], '__typename': 'ProfileFeeds'}}}

    def parse_profile(self, response):

        meta = response.request.meta
        item = {}

        res1 = response.text
        try:
            script = re.search(r'window.__APOLLO_STATE__ ?=([\s\S]*)}}};', res1).group(1)
            # script = re.search(r'window.__APOLLO_STATE__ = (.*?);', res1).group(1)
            # scripts = json.loads(script)  # 需要Cookies, 有可能获取空值导致下面解析错误
            # print(scripts)

            # 解析页面视频详细信息和下一页pcursor
            principalId = re.search('\$User:(.*?)\.verifiedStatus', script).group(1)
            # print('principalId: ', principalId)
            logging.info('principalId: ' + principalId)
            item['principalId'] = principalId

            # name = re.search('"name":"(.*?)"', script).group(1)
            # eid = re.search('"eid":"(.*?)"', script).group(1)

            item['description'] = re.search('"description":"(.*?)"', script).group(1)
            fan = re.search('"fan":"(.*?)"', script).group(1)
            item['fan'] = analy.parsingChar(fan)
            follow = re.search('"follow":"(.*?)"', script).group(1)
            item['follow'] = analy.parsingChar(follow)
            photo = re.search('"photo":"(.*?)"', script).group(1)
            item['photo'] = analy.parsingChar(photo)
            item['sex'] = re.search('"sex":"(.*?)"', script).group(1)
            item['authorId'] = re.search('"userId":"(.*?)"', script).group(1)
            item['cityname'] = re.search('"cityName":"(.*?)"', script).group(1)

            # 敏感词
            content = meta['authorName'] + item['description']
            item['sensitive'] = self.verify_sensitive(content)

            logging.info(item['description']+item['fan']+item['follow']+item['photo']+item['sex']+item['cityname']+item['authorId'])
            self.ks.update({'authorEid': meta['authorEid']}, {'$set': item})  # 插入数据库
            logging.info('视频数量: ' + item['photo'])

            if 7 <= int(item['photo']) <= 1500:  # 视频数量筛选阈值
                url_2 = 'https://live.kuaishou.com/graphql'

                headers = {
                    "Host": "live.kuaishou.com",
                    "Connection": "keep-alive",
                    "accept": "*/*",
                    "Origin": "https://live.kuaishou.com",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
                    "content-type": "application/json",
                    "Referer": "https://live.kuaishou.com/profile/%s" % (item['principalId']),
                    "Accept-Encoding": "gzip, deflate, br",
                    "Accept-Language": "zh-CN,zh;q=0.9",
                    # "Cookie": "clientid=3; did=web_dc309322be1e34669a8dbdb93483950a; client_key=65890b29; kuaishou.live.bfb1s=3e261140b0cf7444a0ba411c6f227d88"
                }

                formdata = {"operationName": "publicFeedsQuery",
                            "variables": {"principalId": meta['authorEid'], "pcursor": "", "count": 24},
                            "query": "query publicFeedsQuery($principalId: String, $pcursor: String, $count: Int) {\n  publicFeeds(principalId: $principalId, pcursor: $pcursor, count: $count) {\n    pcursor\n    live {\n      user {\n        id\n        kwaiId\n        eid\n        profile\n        name\n        living\n        __typename\n      }\n      watchingCount\n      src\n      title\n      gameId\n      gameName\n      categoryId\n      liveStreamId\n      playUrls {\n        quality\n        url\n        __typename\n      }\n      followed\n      type\n      living\n      redPack\n      liveGuess\n      anchorPointed\n      latestViewed\n      expTag\n      __typename\n    }\n    list {\n      photoId\n      caption\n      thumbnailUrl\n      poster\n      viewCount\n      likeCount\n      commentCount\n      timestamp\n      workType\n      type\n      useVideoPlayer\n      imgUrls\n      imgSizes\n      magicFace\n      musicName\n      location\n      liked\n      onlyFollowerCanComment\n      relativeHeight\n      width\n      height\n      user {\n        id\n        eid\n        name\n        profile\n        __typename\n      }\n      expTag\n      __typename\n    }\n    __typename\n  }\n}\n"}

                meta['url_2'] = url_2
                meta['headers'] = headers
                meta['page'] = 1
                meta['request_num'] = 1
                meta['formdata'] = formdata

                yield Request(method='POST', url=url_2, headers=headers, body=json.dumps(formdata),
                              callback=self.parse_video_list,
                              meta=meta,
                              dont_filter=True, priority=9)  # Cookie 失效 {'data': {'publicFeeds': {'pcursor': None, 'live': None, 'list': [], '__typename': 'ProfileFeeds'}}}
            else:
                logging.warning('视频数量少于10个或者大于500, pass...')

        except Exception as e:
            logging.warning('Cookies 可能发生错误, 修改状态为20, 并记入文件...')

            resull = self.ks.update({'authorEid': meta['authorEid']}, {'$set': {'status': 20}})

            with open('ks_getEids_error.txt', 'a+') as f:
                f.write('发生错误的原因是: ' + str(e) + '\n' + '发生错误的id是: ' + meta['authorEid'] + '\n')

    def parse_video_list(self, response):

        meta = response.request.meta
        item = {}

        res1 = response.text
        res = json.loads(res1)

        next_page_pcursor = res['data']['publicFeeds']['pcursor']

        if next_page_pcursor == None:
            print('该用户可能被封禁 %s , 修改状态为10' % (meta['authorEid']))
            logging.warning('该用户可能被封禁 %s' % (meta['authorEid']))

            resul = self.ks.update({'authorEid': meta['authorEid']}, {'$set': {'status': 10}})

        else:
            info_list = res['data']['publicFeeds']['list']
            for info in info_list:
                item['authorEid'] = meta['authorEid']
                item['photoId'] = info.get('photoId')
                item['viewCount'] = analy.parsingChar(info.get('viewCount'))
                item['likeCount'] = analy.parsingChar(info.get('likeCount'))
                item['workType'] = info.get('workType')
                item['imgUrls'] = info.get('imgUrls')
                item['timestamp'] = info.get('timestamp')
                # item['caption'] = info.get('caption') # 暂时用不到, 注释掉
                item['video_url'] = ''  # 视频链接地址
                item['dw'] = 0  # 视频是否下载
                item['dw_url'] = 0  # 视频链接地址是否下载
                item['dw_relative'] = 0  # 视频相关评论和链接是否下载
                # print(item['photoId'], item['viewCount'], item['likeCount'], item['workType'], item['imgUrls'], item['timestamp'], item['caption'])

                # self.ks.insert({'authorEid': meta['authorEid']}, {'$set': item})  # 插入数据库
                if self.ks_video.find_one({'photoId': item['photoId']}):
                    print('数据库已经存在...')
                    logging.info('数据库已经存在...')
                else:
                    print('数据库不存在, 插入...')
                    logging.info('数据库不存在, 插入...')
                    item_tuples = item.copy()
                    result = self.ks_video.insert_one(item_tuples)

            if next_page_pcursor != 'no_more' and meta['page'] < 2:

                meta['page'] += 1
                meta['formdata']['variables']['pcursor'] = next_page_pcursor

                print('正在请求第 %s 页...' % (meta['page']))
                logging.info('正在请求第 %s 页...' % (meta['page']))
                yield Request(method='POST', url=meta['url_2'], headers=meta['headers'], body=json.dumps(meta['formdata']), callback=self.parse_video_list,
                              meta=meta, dont_filter=True)
            else:
                print('视频列表已经抓取完成...')
                logging.info('视频列表已经抓取完成...')

    def verify_sensitive(self, content):

        # print('content:', content)
        minc = ['代购', '后援', '日报', '有限公司', '房屋租售',
                '现货', '珠宝', '后援', '法院', '批发', '专卖', '宠物', '经销', '影视', '政府',
                '手游', '王者荣耀', '绝地求生', '和平精英', '英雄联盟', 'LOL', '穿越火线', '地下城与勇士', '大话西游', '梦幻西游', '球球大作战', '荒野行动',
                '天龙八部', '守望先锋', '只狼', 'CF手游', '神秘海域', '古墓丽影', '问道', '传奇世界', '我的世界', '跑跑卡丁车', '魔兽世界', '坦克世界',
                '炉石传说', '反恐精英', '守望先锋', '流放之路', '地下城与勇士', '游戏主播', 'DNF', '全军出击', '刺激战场',
                '童装', '全网国服', '网游专营店', '明星视频', '娱乐圈', '金毛犬', '美甲', '游戏直播', '铲屎官', '吸猫', '漫画',
                '解说', '斗地主', '说车']  # 车, 汽车, 机车, 漫画, 学校, 女装, 社区, 精选, 日语, 俱乐部, 潮鞋
        res_li = []
        for mi in minc:
            if mi in content:
                res = mi
                break

            else:
                res = ''

        return res

    # if "User:null" not in script:
    # # 解析页面视频详细信息和下一页pcursor
    # principalId = re.search(r'\$User:(.*?)\.verifiedStatus', script).group(1)
    # print('principalId', principalId)
    # item['principalId'] = principalId
    #
    # userinfo = jsonpath(scripts, '$..defaultClient.User:{}'.format(principalId))[0]
    # key = '$User:{}.countsInfo'.format(principalId)
    # usercountsinfo = scripts.get('defaultClient').get(key)
    #
    # item['description'] = userinfo.get('description')
    #
    # item['fan'] = analy.parsingChar(usercountsinfo.get('fan'))
    #
    # item['follow'] = analy.parsingChar(usercountsinfo.get('follow'))
    #
    # item['photo'] = analy.parsingChar(usercountsinfo.get('photo'))
    #
    # item['sex'] = userinfo.get('sex')
    #
    # item['cityname'] = userinfo.get('cityName')
    #
    # item['authorId'] = userinfo.get('userId')